from numpy.core import numeric
from ext import data_mining

import pandas as pd 
from sklearn.datasets import load_iris

import numpy as np


def create_app():
    data = pd.read_csv("data\data_covid_tratado_numeric_09052021.csv") 
    
    # 01 - Selection
    data_target = load_iris()
    print(data_target)


    # 02 - Pre-Procesing
    data_preprocessed = data_target

    # 03 - Transformation
    data_transformed = {}

    
    data_transformed['target_names'] = np.array(["Obito na UTI","Alta da UTI"])
    data_transformed['target'] = np.array(data["Desfecho"].map({"Obito na UTI": 0, "Alta da UTI": 1}))
    data_transformed['feature_names'] = np.array(data.columns)
    data_transformed['data']= np.array(data[['Idade','Diabetes','Hemoglobina','Leucocito','Plaquetas']].fillna(0))


    print(data_transformed['data'])

    # 04 - Data Mining
    patterns = data_mining.train(data_transformed)

    data_mining.export(patterns["model"], "covid_naivebayes")
    model = data_mining.load("iris_naivebayes")
    # patterns = data_mining.load("iris_naivebayes")


    # 05 - Evaluation
    print(patterns["accuracy"])
    print(model)


    return

if __name__ == "__main__":
    create_app()


