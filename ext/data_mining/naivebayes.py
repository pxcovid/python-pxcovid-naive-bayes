# Codigo retirado de https://www.linkedin.com/pulse/criando-um-modelo-preditivo-e-colocando-em-produção-maciel-guimarães/?originalSubdomain=pt

from numpy import PINF
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score


def nb_train(data):

    result = {}

    # Organizar nossos dados
    label_names = data['target_names']
    labels = data['target']
    feature_names = data['feature_names']
    features = data['data']

    # Dividir nossos dados
    train, test, train_labels, test_labels = train_test_split(features,
                                                              labels,
                                                              test_size=0.1,
                                                              random_state=62)
    

    # Inicializar nosso classificador
    gnb = GaussianNB()


    print(train, train_labels)

    # Treinar nosso classificador
    model = gnb.fit(train, train_labels)

    # Fazer previsões
    preds = model.predict(train)

    print(train_labels, preds)


    result["accuracy"] = accuracy_score(train_labels, preds)
    result["model"] = gnb

    return result