from .naivebayes import nb_train
from .ext.joblib import joblib_export, joblib_load


def train(data):
    return nb_train(data)

def export(data, name: str):
    return joblib_export(data, name)

def load(data):
    return joblib_load(data)
