import joblib as joblib

def joblib_export(model, name:str):
    joblib.dump(model, './models/' + name + '.pk1')
    pass

def joblib_load(name:str):
    return joblib.load('./models/' + name + '.pk1')
