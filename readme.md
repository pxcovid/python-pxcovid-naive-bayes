# PxCovid Naive Bayes
Este projeto é um para o processo de KDD e exportação de um modelo para os dados do PxCovid.

# Naive Bayes
Foi retirado de: 
- [Como Construir um Classificador de Machine Learning em Python com Scikit-learn
](https://www.digitalocean.com/community/tutorials/como-construir-um-classificador-de-machine-learning-em-python-com-scikit-learn-pt)
-  [Criando um modelo preditivo e colocando em produção (Machine Learning)](https://www.linkedin.com/pulse/criando-um-modelo-preditivo-e-colocando-em-produção-maciel-guimarães/?originalSubdomain=pt)

# Como Rodar

- ## Local 
    - Requisitos:
        - Python ( _3.9_ )
        - sklearn ( _^_ )

```bash
git clone https://gitlab.com/pxcovid/python-pxcovid-naive-bayes.git

cd python-pxcovid-naive-bayes/
poetry install 
poetry update

poetry run python .\algorithm\naivebayes.py
poetry run python .\naivebayes.py
```