# [1] importo o deserializador
import joblib as joblib

# [2] Carrego a classe de predição do diretório local
clf = joblib.load('./models/covid_naivebayes.pk1')

# [3] Recupero as informações de uma Flor
# sepal_length = float(5.1)
# sepal_width = float(3.5)
# petal_length = float(1.4)
# petal_width = float(0.2)

event = [1,0,3,3,2]
target_names = ['Alta da UTI', 'Obito na UTI']

result = {}


# [4] Realiza predição com base no evento
prediction = clf.predict([event])[0]

# [5] Realizar probabilidades individuais das três classes
probas = zip(target_names, clf.predict_proba([event])[0])

# [6] Recupera o nome real da classe
result['prediction'] = target_names[prediction]
result['probas'] = tuple(probas)

print(result)